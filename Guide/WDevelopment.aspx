﻿<%@ Page Title="Web development" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WDevelopment.aspx.cs" Inherits="Guide.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1 id="page-title">WEB Programming</h1>
    <p>This is a tricky way how to fill an array using loop instead of typing lines of code </p>
     <uctrl:CodeBox runat="server" id="Code" CodeToServe="wProgrammingCode" />
    <p>In this case we interpret string and counter from loop to the variable using method eval()</p>
</asp:Content>
