﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Database.aspx.cs" Inherits="Guide.WebForm3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1 id="page-title">Database Design</h1>
    <p>Here is an example from the book named 'Murach's Oracle SQL and PLSQLfor Developers'</p>
    <uctrl:CodeBox runat="server" id="Code" CodeToServe="DataBaseCode" />
</asp:Content>
